package com.manishp.twittersearch.network;

import com.manishp.twittersearch.model.TweetSearch;
import com.manishp.twittersearch.utility.Constant;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mpatole on 2/8/17.
 */

public interface TweetSearcher {

    @Headers({
            Constant.AUTH_HEADER,
            Constant.CONTENT_HEADER
    })
    @GET(Constant.SEARCH_TWEET)
    Call<TweetSearch> searchTweets(@Query(Constant.QUERY) String query);
}
