package com.manishp.twittersearch.network;

import com.manishp.twittersearch.utility.NetworkService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Component to satisfy dependency injection.
 *
 * @author Manish Patole.
 */
@Singleton
@Component(modules = {NetModule.class})
public interface NetComponent {
  void inject(TwitterNetworkManager networkService);
}
