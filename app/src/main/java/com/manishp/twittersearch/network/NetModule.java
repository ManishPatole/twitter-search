package com.manishp.twittersearch.network;

import com.manishp.twittersearch.utility.Constant;
import com.manishp.twittersearch.utility.NetworkService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module class provides Retrofit client.
 *
 * @author Manish Patole.
 */
@Module
public class NetModule {

  @Singleton
  @Provides
  String providesBaseUrl() {
    return Constant.BASE_URL;
  }

  @Singleton
  @Provides
  Retrofit providesRetrofitClient(String baseUrl) {
    return new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build();
  }

  @Singleton
  @Provides
  TweetSearcher providesTweeterSearcher(Retrofit retrofit) {
    return retrofit.create(TweetSearcher.class);
  }

  @Singleton
  @Provides
  NetworkService providesNetworkService(TweetSearcher tweetSearcher) {
    return new NetworkService(tweetSearcher);
  }
}
