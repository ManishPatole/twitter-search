package com.manishp.twittersearch.network;

import com.manishp.twittersearch.R;
import com.manishp.twittersearch.application.TwitterApplication;
import com.manishp.twittersearch.model.TweetSearch;
import com.manishp.twittersearch.utility.NetworkService;

import java.lang.ref.Reference;

import javax.inject.Inject;

import retrofit2.Call;

/**
 * Handles network operation.
 *
 * @author Manish Patole.
 */
public class TwitterNetworkManager implements ExecutorCallback.OnServerCallResponse<TweetSearch> {

  private final OnTweetSearch mOnTweetSearch;
  @Inject
  NetworkService networkService;

  public TwitterNetworkManager(OnTweetSearch mOnTweetSearch) {
    this.mOnTweetSearch = mOnTweetSearch;

    NetComponent netComponent = DaggerNetComponent.create();
        // list of modules that are part of this component need to be created here too
        // This also corresponds to the name of your module: %component_name%Module
//        .netModule(new NetModule())
//        .build();
    netComponent.inject(this);
  }

  public void searchTweets(String query) {
    final Call<TweetSearch> tweetSearchCall = networkService.getTweetSearcher().searchTweets(query);
    tweetSearchCall.enqueue(new ExecutorCallback<TweetSearch>(this));
  }

  @Override
  public void onSuccess(TweetSearch response) {
    mOnTweetSearch.onSuccess(response);
  }

  @Override
  public void onFailure(Throwable throwable) {
    if (null != mOnTweetSearch) {
      mOnTweetSearch.onFailure(null == throwable ? TwitterApplication.getTweeterApp().getString(R
          .string.something_went_wrong) : throwable.getMessage());
    }
  }

  @Override
  public void onFailure() {
    if (null != mOnTweetSearch) {
      mOnTweetSearch.onFailure(TwitterApplication.getTweeterApp().getString(R
          .string.something_went_wrong));
    }
  }

  public interface OnTweetSearch {

    void onSuccess(TweetSearch tweetSearch);

    void onFailure(String error);
  }
}
