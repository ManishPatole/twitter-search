package com.manishp.twittersearch;

import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.manishp.twittersearch.adapter.TwitterAdapter;
import com.manishp.twittersearch.base.BaseActivity;
import com.manishp.twittersearch.model.Status;
import com.manishp.twittersearch.presenter.TwitterSearchPresenterImpl;
import com.manishp.twittersearch.utility.Constant;
import com.manishp.twittersearch.utility.DialogUtil;
import com.manishp.twittersearch.utility.NetworkStateReceiver;
import com.manishp.twittersearch.utility.Utility;
import com.manishp.twittersearch.view.TweetView;

import java.util.List;

/**
 * Represents the UI to interact. User can search tweets giving inputs.
 * The screen will refresh after every 5 seconds.
 *
 * @author Manish Patole.
 */
public class TwitterSearchActivity extends BaseActivity<TwitterSearchPresenterImpl>
    implements TweetView, View.OnClickListener {

  private RecyclerView rvTweetList;
  private EditText etSearchText;
  private Button btSearch;

  private TwitterAdapter mTwitterAdapter;
  private NetworkStateReceiver mNetworkStateReceiver = new NetworkStateReceiver();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.animated_activity_search);
    intiView();
  }

  @Override
  protected void onResume() {
    super.onResume();
    registerReceiver(mNetworkStateReceiver, new IntentFilter(Constant.ACTION_CONNECTIVITY));
  }

  /**
   * Initialises the UI components.
   */
  private void intiView() {
    rvTweetList = (RecyclerView) findViewById(R.id.tweet_list);
    etSearchText = (EditText) findViewById(R.id.search_text);
    btSearch = (Button) findViewById(R.id.search);
    btSearch.setOnClickListener(this);

    mTwitterAdapter = new TwitterAdapter(null);
    rvTweetList.setLayoutManager(new LinearLayoutManager(this));
    rvTweetList.setHasFixedSize(true);
    rvTweetList.setAdapter(mTwitterAdapter);
  }

  @Override
  protected TwitterSearchPresenterImpl getPresenter() {
    return new TwitterSearchPresenterImpl(this);
  }

  @Override
  public void showProgress() {
    rvTweetList.setVisibility(View.GONE);
    Utility.showProgressDialog(this);
  }

  @Override
  public void hideProgress() {
    rvTweetList.setVisibility(View.VISIBLE);
    Utility.hideProgressDialog();
  }

  @Override
  public void refreshList(final List<Status> statusList) {
    mTwitterAdapter.refreshList(statusList);
  }

  @Override
  public void showSearchError(String error) {
    DialogUtil.showErrorDialog(this, getString(R.string.error), error, this);
  }

  @Override
  public void showEditTextError(String error) {
    etSearchText.setError(error);
    etSearchText.requestFocus();
  }

  /**
   * Starts the search.
   */
  private void startSearch() {
    if (checkInternet()) {
      mPresenter.getTweets(etSearchText.getText().toString());
    }
  }

  @Override
  public void onPositiveButtonClick(DialogInterface dialog) {
    dialog.dismiss();
  }

  @Override
  protected void onStop() {
    super.onStop();
    mPresenter.shutdownScheduling();
    unregisterReceiver(mNetworkStateReceiver);
  }

  /**
   * Hides the open keyboard.
   */
  private void hideKeyboard() {
    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(btSearch.getWindowToken(), 0);
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.search:
        hideKeyboard();
        startSearch();
        break;
    }
  }
}
