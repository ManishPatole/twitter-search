package com.manishp.twittersearch.view;

import com.manishp.twittersearch.base.IView;
import com.manishp.twittersearch.model.Status;

import java.util.List;

/**
 * Created by mpatole on 2/8/17.
 */

public interface TweetView extends IView {

    void showProgress();

    void hideProgress();

    void refreshList(List<Status> statusList);

    void showSearchError(String error);

    void showEditTextError(String error);

}
