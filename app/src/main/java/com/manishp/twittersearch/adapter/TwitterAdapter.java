package com.manishp.twittersearch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.manishp.twittersearch.R;
import com.manishp.twittersearch.model.Status;
import com.manishp.twittersearch.model.User;
import com.manishp.twittersearch.utility.Utility;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Adapter to twitter list.
 *
 * @author Manish Patole.
 */
public class TwitterAdapter extends RecyclerView.Adapter<TwitterAdapter.ViewHolder> {

  private List<Status> mStatuses;

  public TwitterAdapter(List<Status> statusList) {
    mStatuses = statusList;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.twitter_row, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    final Status status = mStatuses.get(position);

    holder.tvDescription.setText(status.getText());
    holder.tvReTweetCount.setText(String.valueOf(status.getRetweetCount()));
    holder.tvFavCount.setText(String.valueOf(status.getFavoriteCount()));

    try {
      final Date date = Utility.sCreatedDate.parse(status.getCreatedAt());
      holder.tvDate.setText(Utility.sDisplayDate.format(date));

    } catch (ParseException e) {
      e.printStackTrace();
    }

    User user = status.getUser();
    if (null != user) {
      holder.tvName.setText(user.getName());
      holder.tvNickName.setText("@" + user.getScreenName());
    }
  }

  @Override
  public int getItemCount() {
    return null != mStatuses ? mStatuses.size() : 0;
  }

  public void refreshList(List<Status> statusList) {
    mStatuses = statusList;
    notifyDataSetChanged();
  }

  class ViewHolder extends RecyclerView.ViewHolder {

    ImageView ivAvatar;
    TextView tvDescription;
    TextView tvName;
    TextView tvNickName;
    TextView tvDate;
    TextView tvReTweetCount;
    TextView tvFavCount;
    View vParentView;

    ViewHolder(View view) {
      super(view);

      vParentView = view.findViewById(R.id.parent_view);
      ivAvatar = (ImageView) view.findViewById(R.id.avatar);
      tvName = (TextView) view.findViewById(R.id.user_name);
      tvNickName = (TextView) view.findViewById(R.id.nick_name);
      tvDescription = (TextView) view.findViewById(R.id.description);
      tvDate = (TextView) view.findViewById(R.id.date);
      tvReTweetCount = (TextView) view.findViewById(R.id.retweet_count);
      tvFavCount = (TextView) view.findViewById(R.id.fav_count);
    }
  }
}