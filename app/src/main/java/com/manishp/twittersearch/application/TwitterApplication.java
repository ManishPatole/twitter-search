package com.manishp.twittersearch.application;

import android.app.Application;

/**
 * Application class.
 *
 * @author Manish Patole.
 */
public class TwitterApplication extends Application {

  private static TwitterApplication mTweeterApp;

  @Override
  public void onCreate() {
    super.onCreate();
    mTweeterApp = this;
  }

  public static TwitterApplication getTweeterApp() {
    return mTweeterApp;
  }
}
