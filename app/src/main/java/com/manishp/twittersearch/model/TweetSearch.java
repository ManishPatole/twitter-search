package com.manishp.twittersearch.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Holds tweets.
 *
 * @author Manish Patole.
 */

public class TweetSearch {

  @SerializedName("statuses")
  @Expose
  private List<Status> statuses = null;
  @SerializedName("search_metadata")
  @Expose
  private SearchMetadata searchMetadata;

  public List<Status> getStatuses() {
    return statuses;
  }

  public void setStatuses(List<Status> statuses) {
    this.statuses = statuses;
  }

  public SearchMetadata getSearchMetadata() {
    return searchMetadata;
  }

  public void setSearchMetadata(SearchMetadata searchMetadata) {
    this.searchMetadata = searchMetadata;
  }
}
