package com.manishp.twittersearch.utility;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Dialog utility.
 *
 * @author Manish Patole.
 */

public class DialogUtil {

  public static void showErrorDialog(Context context, String title, String message,
                                     final OnDialogClick onDialogClick) {
    new AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            if (null != onDialogClick) {
              onDialogClick.onPositiveButtonClick(dialog);
            } else {
              dialog.dismiss();
            }
          }
        })
        .setIcon(android.R.drawable.ic_dialog_alert)
        .show();
  }

  public interface OnDialogClick {
    void onPositiveButtonClick(DialogInterface dialog);
  }
}
