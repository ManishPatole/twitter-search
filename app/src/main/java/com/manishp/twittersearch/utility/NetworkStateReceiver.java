package com.manishp.twittersearch.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Network status receiver.
 *
 * @author Manish Patole.
 */
public class NetworkStateReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.i("NetworkStateReceiver", "onReceive");
    Utility.isConnectedToInternet = Utility.isConnectedToInternet(context);
  }
}
