package com.manishp.twittersearch.utility;

/**
 * Holds constants.
 *
 * @author Manish Patole.
 */
public class Constant {
  public static final String BASE_URL = "https://api.twitter.com/1.1/";
  public static final String SEARCH_TWEET = "search/tweets.json";
  public static final String QUERY = "q";
  public static final String CONTENT_HEADER = "Content-Type: application/json";
  public static final String AUTH_HEADER =
      "Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAPlkzAAAAAAAaFWGkrwY8MKpTCo%2B0KoU7e1ySoo%3DDn6od72DPqiJxh" +
          "24oUX9ygoUWsDJ6LNqFHYSFeQCk7tSvC1n9Y";

  public static final String ACTION_CONNECTIVITY = "android.net.conn.CONNECTIVITY_CHANGE";
}
