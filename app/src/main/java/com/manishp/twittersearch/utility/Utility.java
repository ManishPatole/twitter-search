package com.manishp.twittersearch.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.manishp.twittersearch.R;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Locale;

/**
 * Utility class.
 *
 * @author Manish Patole.
 */
public class Utility {

  private static final String TAG = Utility.class.getSimpleName();
  public static SimpleDateFormat sCreatedDate =
      new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy", Locale.US);
  public static SimpleDateFormat sDisplayDate =
      new SimpleDateFormat("hh:mm:ss aaa - d MMM yyyy", Locale.US);
  private static ProgressDialog sProgressDialog;
  public static boolean isConnectedToInternet;

  public static boolean isEmpty(final Collection<?> coll) {
    return coll == null || coll.isEmpty();
  }

  /**
   * Shows progress dialog.
   *
   * @param context - Live context.
   */
  public static void showProgressDialog(Context context) {
    if (null != sProgressDialog && sProgressDialog.isShowing()) {
      sProgressDialog.hide();
    }
    sProgressDialog = new ProgressDialog(context);

    sProgressDialog.setMessage(context.getString(R.string.loading));
    sProgressDialog.setIndeterminate(true);
    sProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    sProgressDialog.setCancelable(false);

    if (context instanceof Activity) {
      if (!((Activity) context).isFinishing()) {
        sProgressDialog.show();
      }
    } else {
      sProgressDialog.show();
    }
  }

  /**
   * Hides the progress dialog.
   */
  public static void hideProgressDialog() {
    try {
      if (null != sProgressDialog && sProgressDialog.isShowing()) {

        Context context = sProgressDialog.getContext();

        if (context instanceof Activity) {

          if (!((Activity) context).isFinishing()) {
            sProgressDialog.dismiss();
            sProgressDialog = null;
          }
        } else {
          sProgressDialog.dismiss();
          sProgressDialog = null;
        }
      }
    } catch (IllegalArgumentException e) {
      Log.w(TAG, "Simple ignore the exceprion", e);
    }
  }

  /**
   * Checks internet.
   *
   * @param context - live context.
   * @return - status of internet.
   */
  public static boolean isConnectedToInternet(Context context) {

    ConnectivityManager connectivityManager =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Network[] networks = connectivityManager.getAllNetworks();
      NetworkInfo networkInfo;
      for (Network mNetwork : networks) {
        networkInfo = connectivityManager.getNetworkInfo(mNetwork);

        if (networkInfo != null && networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
          return true;
        }
      }
    } else {
      if (connectivityManager != null) {
        //noinspection deprecation
        NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
        if (info != null) {
          for (NetworkInfo networkInfo : info) {
            if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
}
