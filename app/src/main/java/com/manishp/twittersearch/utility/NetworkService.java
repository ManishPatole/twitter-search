package com.manishp.twittersearch.utility;

import com.manishp.twittersearch.model.TweetSearch;
import com.manishp.twittersearch.network.DaggerNetComponent;
import com.manishp.twittersearch.network.ExecutorCallback;
import com.manishp.twittersearch.network.NetComponent;
import com.manishp.twittersearch.network.NetModule;
import com.manishp.twittersearch.network.TweetSearcher;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Network utility.
 *
 * @author Manish Patole.
 */

public class NetworkService {

  private TweetSearcher tweetSearcher;

  public NetworkService(TweetSearcher tweetSearcher) {
    this.tweetSearcher = tweetSearcher;
  }

  public TweetSearcher getTweetSearcher() {
    return tweetSearcher;
  }
}
