package com.manishp.twittersearch.presenter;

import com.manishp.twittersearch.base.IPresenter;

/**
 * Created by mpatole on 2/8/17.
 */

interface TwitterSearchPresenter extends IPresenter {

    void getTweets(String searchText);

    void shutdownScheduling();
}
