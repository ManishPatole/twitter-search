package com.manishp.twittersearch.presenter;

import android.text.TextUtils;

import com.manishp.twittersearch.R;
import com.manishp.twittersearch.application.TwitterApplication;
import com.manishp.twittersearch.model.Status;
import com.manishp.twittersearch.model.TweetSearch;
import com.manishp.twittersearch.network.TwitterNetworkManager;
import com.manishp.twittersearch.utility.Utility;
import com.manishp.twittersearch.view.TweetView;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Initiates and handles the tweeter search.
 *
 * @author Manish Patole.
 */
public class TwitterSearchPresenterImpl
    implements TwitterSearchPresenter, TwitterNetworkManager.OnTweetSearch {

  private static final long DELAY = 5;
  private static final long INITIAL_DELAY = 0;
  private static final String HASH = "%23";
  private TweetView mTweetView;

  private TwitterNetworkManager mTwitterNetworkManager;
  private ScheduledExecutorService mScheduledExecutorService;

  public TwitterSearchPresenterImpl(TweetView mTweetView) {
    this.mTweetView = mTweetView;
    mTwitterNetworkManager = new TwitterNetworkManager(this);
  }

  @Override
  public void getTweets(String searchText) {
    if (null != mTweetView) {
      if (!TextUtils.isEmpty(searchText)) {
        mTweetView.showProgress();
        shutdownScheduling();
        startFetchTweetEngine(searchText);
      } else {
        mTweetView.showEditTextError(
            TwitterApplication.getTweeterApp().getString(R.string.error_enter_text));
      }
    }
  }

  @Override
  public void shutdownScheduling() {
    if (null != mScheduledExecutorService) {
      mScheduledExecutorService.shutdown();
    }
  }

  private void createPool() {
    mScheduledExecutorService = Executors.newScheduledThreadPool(0);
  }

  private void startFetchTweetEngine(final String searchText) {
    createPool();
    mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
      public void run() {
        if (Utility.isConnectedToInternet) {
          mTwitterNetworkManager.searchTweets(HASH + searchText);
        }
      }
    }, INITIAL_DELAY, DELAY, TimeUnit.SECONDS);
  }

  @Override
  public void onSuccess(TweetSearch tweetSearch) {

    if (null != mTweetView) {
      mTweetView.hideProgress();
      if (null != tweetSearch) {
        final List<Status> statuses = tweetSearch.getStatuses();

        if (!Utility.isEmpty(statuses)) {
          mTweetView.refreshList(statuses);

        } else if (Utility.isEmpty(statuses)) {
          shutdownScheduling();
          mTweetView.showSearchError(
              TwitterApplication.getTweeterApp().getString(R.string.failed_to_search));

        } else {
          shutdownScheduling();
          mTweetView.showSearchError(
              TwitterApplication.getTweeterApp().getString(R.string.something_went_wrong));
        }
      }
    }
  }

  @Override
  public void onFailure(String error) {
    if (null != mTweetView) {
      shutdownScheduling();
      mTweetView.hideProgress();
      mTweetView.showSearchError(error);
    }
  }
}
